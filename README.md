<!--
- Filename:     README.md
- Path:         /README.md
- Created Date: Wednesday, November 10th 2021, 12:59:36 pm
- Author:       Robert Hall
- -----
- Copyright (c) 2022 Robert Hall
- -----
- Last Modified: Wed Feb 02 2022
- Modified By: Robert Hall
-->

# DND Stats

One of the dangers for nerds in a game like Dungeons & Dragons is the trap of
wondering about probability distributions, the effects of homebrew mechanics,
and the min-max strategies seen on forums.

In this repository I explore various situations and attempt to either compute
answers to these questions or lay out an analytical foundation to explore them.
I hope they are as fun to read as they have been to create.
